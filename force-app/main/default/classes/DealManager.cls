public inherited sharing class DealManager {
	public static final String DEFAULT_STATUS = 'Open';
    public static final Id LEASE_RECORD_TYPE_ID = Schema.SObjectType.Deal__c.getRecordTypeInfosByName()
                                                                        .get('Lease').getRecordTypeId();
    public static final Id SALE_RECORD_TYPE_ID = Schema.SObjectType.Deal__c.getRecordTypeInfosByName()
                                                                        .get('Sale').getRecordTypeId();

    
	public class DealWrapper {
		public Id id {get; set;}
		public Id Owner {get; set;}
        public Id Customer {get; set;}
        public Id Saler {get; set;}
        public Id SellingProperty {get; set;}
        public String PropertyAddress {get; set;}
        public String Status {get; set;}
        public Date LeaseEndDate {get; set;}
        public Date LeaseStartDate {get; set;}
        public Decimal LeaseMounth {get; set;}
        public Decimal DealAmount {get; set;}
        public Decimal TotalCommissionPercent {get; set;}
        public Decimal SumAfterCommissions {get; set;}
        public Id RecordTypeId {get; set;}
	}

	public static List<Deal__c> getDealsByIds(List<Id> ids, String fields) {
		return Database.query('SELECT ' + fields + ' FROM Deal__c WHERE Id IN :ids');
	}

	public static List<Deal__c> getDealsByRecordTypes(List<Id> recordTypes, String fields) {
		return Database.query('SELECT ' + fields + ' FROM Deal__c WHERE RecordTypeId IN :recordTypes');
	}

    public static List<Deal__c> getDealsByPeriodAndType(String propId, Date startDate, Date endDate, String recTypeName, String fields) {
        Id recTypeId = Schema.SObjectType.Deal__c.getRecordTypeInfosByName().get(recTypeName).getRecordTypeId();
        return Database.query('SELECT ' + fields + ' FROM Deal__c WHERE Selling_Property__c = :propId AND (' 
                                                            + '(RecordTypeId = :recTypeId AND Selling_Property__c >= :startDate AND Lease_end_date__c <= :endDate)'
                                                            + ' OR (RecordTypeId = :recTypeId AND CreatedDate >= :startDate AND CreatedDate <= :endDate)'
                                                            + ')');
    }

	public static Deal__c createDeal(DealWrapper wrapper, Boolean serialize) {
		Deal__c deal = new Deal__c();
        deal.Lease_End_Date__c = wrapper.LeaseEndDate;
        deal.Lease_Start_Date__c = wrapper.LeaseStartDate;
        				
		if (serialize) insert deal;

		return deal;
	}

	public static Deal__c updateDeal(String id, DealWrapper newDeal, Boolean serialize) {
		try {
			Deal__c queriedDeal = [SELECT Selling_Property__c, Property_Address__c  FROM Deal__c WHERE Id=: id];

			queriedDeal.Selling_Property__c	= newDeal.SellingProperty;
			update queriedDeal;
			return queriedDeal;
        } catch(Exception e) {
            System.debug('An unexpected error has occurred: ' + e.getMessage());
			return null;
        }
    }

	public static Deal__c updateDealsById(DealWrapper wrapper, Boolean serialize) {
        Deal__c dealToUpdate = [SELECT Saler__c, Selling_Property__c, Customer__c,
                                            Status__c, RecordTypeId, Lease_Start_Date__c, 
                                            Lease_End_Date__c FROM Deal__c WHERE Id = :wrapper.id];
        
        dealToUpdate.Saler__c = wrapper.saler;
        dealToUpdate.Selling_Property__c = wrapper.sellingProperty;
        dealToUpdate.Customer__c = wrapper.customer;
        dealToUpdate.Status__c = String.isBlank(wrapper.status) ? DEFAULT_STATUS : wrapper.status;
        dealToUpdate.RecordTypeId = wrapper.recordTypeId;
        if (dealToUpdate.RecordTypeId == LEASE_RECORD_TYPE_ID) {
            dealToUpdate.Lease_Start_Date__c = wrapper.leaseStartDate;
            dealToUpdate.Lease_End_Date__c = wrapper.leaseEndDate;
        }

        if (serialize) update dealToUpdate;

        return dealToUpdate;
    }

	public static void deleteDealsByAddress(List<String> address) {
		delete [SELECT Id FROM Deal__c WHERE Property_Address__c IN: address];
	} 
      
    public static List<Deal__c> getDealsBySalerId(String contactId, Set<String> fields) {
        if (String.isBlank(contactId) || fields == null || fields.isEmpty()) {
            return new List<Deal__c>();
        }

        String query = 'SELECT ' + String.join(new List<String>(fields), ',') +
                ' FROM Deal__c' +
                ' WHERE Saler__c = :contactId';
       
        return Database.query(query);
    }
}