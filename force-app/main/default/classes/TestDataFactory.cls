@IsTest
public class TestDataFactory {

    public static final Id DEFAULT_CONTACT_TYPE = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Broker').getRecordTypeId();
    public static final Id DEFAULT_DEAL_TYPE = Schema.SObjectType.Deal__c.getRecordTypeInfosByName().get('Sale').getRecordTypeId();
    public static final Id LEASE_DEAL_TYPE = Schema.SObjectType.Deal__c.getRecordTypeInfosByName().get('Lease').getRecordTypeId();
    public static final Id DEFAULT_PROPERTY_TYPE = Schema.SObjectType.Property__c.getRecordTypeInfosByName().get('Industrial').getRecordTypeId();

    public static final String DEFAULT_DEAL_STATUS = 'Open';
    public static final String DEFAULT_PHONE = '0123456789';
    public static final String DEFAULT_EMAIL = 'test@test.com';
    public static Integer recordCounter = 0; 

    public static Log__c createLog() {
        Log__c log = new Log__c(Type__c = 'Undefined', Message__c = 'Undefined');

        insert log;
        return log;
    }

    public static Log__c createLog(String type, String message) {
        Log__c log = new Log__c(Type__c = type, Message__c = message);

        insert log;
        return log;
    }

    public static List<Log__c> createLogs(Integer count, String type, String message) {
        List<Log__c> logs = new List<Log__c>(count);
        for (Integer i = 0; i < count; i++) {
            logs[i] = new Log__c(Type__c = type, Message__c = message);
        }

        insert logs;
        return logs;
    }

    public static LogLWC__c createLogLWC(String objectType, 
                                        String actionType, 
                                        String description, 
                                        Boolean isSuccessful, 
                                        String errorMessage
    ) {
        LogLWC__c log = new LogLWC__c(); 
        log.ObjectType__c = objectType;
        log.ActionType__c = actionType;
        log.Description__c = description;
        log.IsSuccessful__c = isSuccessful;
        log.ErrorMessage__c = errorMessage;

        insert log; 
        return log;
    }
}

    public static Contact createContact(String firstName, String lastName, String phone, String email, Id recordTypeId) {
        Contact contact = new Contact(
                                    FirstName = firstName, 
                                    LastName = lastName, 
                                    Phone = String.isBlank(phone) ? DEFAULT_PHONE : phone, 
                                    Email = String.isBlank(email) ? DEFAULT_EMAIL + recordCounter++ : email, 
                                    RecordTypeId = String.isBlank(recordTypeId) ? DEFAULT_CONTACT_TYPE : recordTypeId 
                                    );

        insert contact;
        return contact;
    }

    public static List<Contact> createContacts(Integer count, String recordTypeId) {
        List<Contact> contacts = new List<Contact>(count);
        for (Integer i = 0; i < count; i++) {
            contacts[i] = new Contact(
                                    FirstName = 'test', 
                                    LastName = 'contact' + recordCounter, 
                                    Phone = DEFAULT_PHONE,
                                    Email = DEFAULT_EMAIL + recordCounter++,
                                    RecordTypeId = String.isBlank(recordTypeId) ? DEFAULT_CONTACT_TYPE : recordTypeId 
                                    );
        }

        insert contacts;
        return contacts;
    }    
    
    public static Deal__c createDeal(Contact saler, Contact customer, Property__c property, String status,
                                        String recordTypeId, String leaseStart, String leaseEnd) {
        Deal__c deal = new Deal__c(
                                Saler__c = saler.Id, 
                                Customer__c = Customer.Id, 
                                Selling_Property__c = property.Id,
                                Status__c = String.isBlank(status) ? DEFAULT_DEAL_STATUS : status, 
                                RecordTypeId = String.isBlank(recordTypeId) ? DEFAULT_DEAL_TYPE : recordTypeId
                                );
        if (deal.RecordTypeId == LEASE_DEAL_TYPE) {
            deal.Lease_Start_Date__c = Date.valueOf(leaseStart);
            deal.Lease_End_Date__c = Date.valueOf(leaseEnd);
        }

        insert deal;
        return deal;
    }

    public static List<Deal__c> createDeals(Integer count, Contact saler, Contact customer, Property__c property, String status,
                                                String recordTypeId, String leaseStart, String leaseEnd) {
        List<Deal__c> deals = new List<Deal__c>(count);
        for (Integer i = 0; i < count; i++) {
            deals[i] = new Deal__c(
                                Saler__c = saler.Id, 
                                Customer__c = customer.Id, 
                                Selling_Property__c = property.Id, 
                                Status__c = String.isBlank(status) ? DEFAULT_DEAL_STATUS : status,  
                                RecordTypeId = String.isBlank(recordTypeId) ? DEFAULT_DEAL_TYPE : recordTypeId
                                );
            if (deals[i].RecordTypeId == LEASE_DEAL_TYPE) {
                deals[i].Lease_Start_Date__c = Date.valueOf(leaseStart);
                deals[i].Lease_End_Date__c = Date.valueOf(leaseEnd);
            }
        }

        insert deals;
        return deals;
    }

    public static Property__c createProperty(Contact propertyOwner, Decimal sellingPrice, Decimal rentCost, String address, String recordTypeId) {
        Property__c p = new Property__c(
                                    Property_Owner__c = propertyOwner.Id, 
                                    Selling_price__c = sellingPrice, 
                                    Rent_Cost__c = rentCost, 
                                    Address__c = address,
             					    RecordTypeId = String.isBlank(recordTypeId) ? DEFAULT_PROPERTY_TYPE : recordTypeId
                                    );

        insert p;
        return p;
    }

    public static List<Property__c> createProperties(Integer count, Contact propertyOwner) {
        List<Property__c> properties = new List<Property__c>(count);
        for (Integer i = 0; i < count; i++) {
            properties[i] = new Property__c(
                                        Property_Owner__c = propertyOwner.Id, 
                                        Selling_price__c = count * i * (count + i), 
                                        Rent_Cost__c = count * i * (count - i + 1), 
                                        Address__c = 'Property address' + recordCounter++);
        }

        insert properties;
        return properties;
    }    

     public User CreateUser()
    {
        Profile profile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];           
        User user = new User(
                            LastName = 'UserLastName', 
                            Alias = 'newuser', 
                            Email ='user@gmail.com', 
                            Username ='NewUser@gmail.com',
                            EmailEncodingKey ='UTF-8', 
                            LanguageLocaleKey ='en_US', 
                            LocaleSidKey ='en_US', 
                            ProfileId = profile.Id,
                            TimeZoneSidKey = 'America/Los_Angeles'
                            );
         
        insert user;                        
        return user;
    }
    
    public User CreateUser(string userType)
    {
        Profile profile = [SELECT Id FROM Profile WHERE Name = :userType];
        User user = new User(
                            LastName = 'UserType', 
                            Alias = 'usertype', 
                            Email ='usertype@gmail.com', 
                            Username ='UserType@gmail.com',
                            EmailEncodingKey ='UTF-8', 
                            LanguageLocaleKey ='en_US', 
                            LocaleSidKey ='en_US', 
                            ProfileId = profile.Id,
                            TimeZoneSidKey = 'America/Los_Angeles'
                            );
        insert user; 
        return user;
    }
}