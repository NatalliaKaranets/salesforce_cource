@IsTest
private class LogManager_Test {

    private static final String DEBUG = 'DEBUG';

    @TestSetup static void setup() {
        TestDataFactory.createLog(DEBUG, 'Test message');
    }

    @IsTest static void testCreateLog() {
        Log__c log = LogManager.log(DEBUG, 'The code is working correctly', false);
        Test.startTest();
        Database.SaveResult result = Database.insert(log);
        Test.stopTest();
        System.assert(result.isSuccess());
    }

    @IsTest static void testGetLogsByIds() {
        Id logId = [SELECT Id FROM Log__c LIMIT 1].id;
        System.assertEquals(1, LogManager.getLogsByIds(new List<Id>{logId}, 'Id').size());
    }

    @IsTest static void testGetLogsByTypes() {
        String type = [SELECT Type__c FROM Log__c WHERE Type__c = :DEBUG].Type__c;
        System.assertEquals(1, LogManager.getLogsByTypes(new List<String>{type}, 'Id').size());
    }

    @IsTest static void testDeleteLogsByTypes() {
        LogManager.deleteLogsByTypes(new List<String>{DEBUG});
        System.assertEquals(0, [SELECT COUNT() FROM Log__c WHERE Type__c = :DEBUG]);
    }

    @IsTest static void testUpdateLogsTypesByIds() {
        Id logId = [SELECT Id FROM Log__c WHERE Type__c = :DEBUG].Id;
        List<Log__c> updatedList = LogManager.updateLogsTypesByIds(new List<Id>{logId}, 'FINEST',true);
        System.assertEquals(1, updatedList.size());
    }
}
