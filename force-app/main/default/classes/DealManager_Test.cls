@isTest

public with sharing class DealManager_Test {
    
     @IsTest static void getDealsbyIdTest(){      
        list<Id> ids = new list<Id>();
        ids.add('a055i00000F4ga5AAB');
        list<Deal__c> dlList = DealManager.getDealsbyIds(ids, 'Number');
        System.debug(dlList);
   }
    
    @IsTest static void createDealTest(){
        DealManager.DealWrapper deal = new DealManager.DealWrapper();
        deal.LeaseEndDate = date.newInstance(1991, 11, 21);
        deal.LeaseStartDate = date.newInstance(1990, 11, 21);
        Deal__c dl = DealManager.createDeal (deal, true);
       
    }
     
    @IsTest static void deleteDealsByAddressTest() {
		list<String> address = new list<String>();
        address.add('Test');
        DealManager.deleteDealsByAddress(address);
        System.assertEquals(address, null);
    }
}    