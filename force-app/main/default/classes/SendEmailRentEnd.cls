public class SendEmailRentEnd {
    public static void sendEmailsRemindEndRent(List<Deal__c> deals) {
            List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
            for (Deal__c deal : deals) {
                Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();
                email.setReplyTo(deal.Client__r.Email);
                email.setSenderDisplayName(deal.Client__r.Email);
                email.setTargetObjectId(deal.Id);
                email.setSaveAsActivity(true);
                email.setTemplateId('Notifying_client_lease_expiring.Id');
                allmsg.add(email);
            }
    
            Messaging.sendEmail(allmsg, false);
    }
}
