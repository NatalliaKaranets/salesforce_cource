public class GetDataByLease {

    public class Info {
        public Datetime createDate {get; set;}
        public Date rentStartDate {get; set;}
        public Date rentEndDate {get; set;}
        public Client client {get; set;}
        public Property property {get; set;}    

        public Info (Deal__c deal) {
            this.createDate = deal.CreatedDate;
            this.rentStartDate = deal.Lease_start_date__c;
            this.rentEndDate = deal.Lease_end_date__c;
            this.client = new Client(deal.Client__r);
            this.property = new Property(deal.Selling_Property__r);
        }
    }

    public class Client {
        public String sfId {get; set;}
        public String firstName {get; set;}
        public String lastName {get; set;}
        public String email {get; set;}

        public Client(Contact client) {
            this.sfId = client.Id;
            this.firstName = client.FirstName;
            this.lastName = client.LastName;
            this.email = client.Email;
        }
    }

    public class Property {
        public String sfId {get; set;}
        public Client owner {get; set;}
        public LocationWrapper location {get; set;}

        public Property(Property__c property) {
            this.sfId = property.Id;
            this.owner = new client(property.Property_Owner__r);
            this.location = new LocationWrapper(property);
        }
    }

    public class LocationWrapper {
        public String country {get; set;}
        public String city {get; set;}
        public String address {get; set;}
        public String latitude   {get; set;}
        public String longitude {get; set;}

        public LocationWrapper(Property__c property) {
            this.country = property.Country__c;
            this.city = property.City__c;
            this.address = property.Address__c;
            this.latitude = String.valueOf(property.Latitude__c);
            this.longitude = String.valueOf(property.Longitude__c);
        }
    }
     public class PropertyGETWrapper {
        public String Id {get; set;}
        public String country {get; set;}
        public String city {get; set;}
        public String address {get; set;}
        public String latitude   {get; set;}
        public String longitude {get; set;}

        public PropertyGETWrapper(Property__c property) {
            this.Id = property.Id;
            this.country = property.Country__c;
            this.city = property.City__c;
            this.address = property.Address__c;
            this.latitude = String.valueOf(property.Latitude__c);
            this.longitude = String.valueOf(property.Longitude__c);
        }
    }

    public class PropertyPOSTWrapper {
        public String id {get; set;}
        public String country {get; set;}
        public String city {get; set;}
        public String address {get; set;}
        public String latitude {get; set;}
        public String longitude {get; set;}
        public String propertyOwner {get; set;}
        public String rentCost {get; set;}
        public String sellingPrice {get; set;}
    }

    public class UpdatePropertyOwnerPUTWrapper {
        public String propertyId {get; set;}
        public Client newOwner {get; set;}
    }

    public class Response {
        public Date startDate {get; set;}
        public Date endDate {get; set;}
        public String totalRevenue {get; set;}
        public List<DealWrapper> deals {get; set;}

        public Response (Date startDate, Date endDate, List<Deal__c> deals) {
            this.startDate = startDate;
            this.endDate = endDate;
            this.deals = new List<DealWrapper>();
            Decimal revenue = 0;
            for (Deal__c deal : deals) {
                this.deals.add(new DealWrapper(deal));
                revenue += deal.Total_Revenue__c;
            }

            this.totalRevenue = String.valueOf(revenue);
        }
    }

    public class DealWrapper {
        public String sfId {get; set;}
        public String dealType {get; set;}
        public GetDataByLease.Client realtor {get; set;}
        public Decimal revenue {get; set;}
        public String clientId {get; set;}
        public String propertyId {get; set;}

        public DealWrapper (Deal__c deal) {
            this.sfId = deal.Id;
            this.dealType = deal.RecordType.DeveloperName;
            this.realtor = new Client(deal.Broker__r);
            this.revenue = deal.Sum_After_Commissions__c;
            this.clientId = deal.Client__r.Id;
            this.propertyId = deal.Selling_Property__r.Id;
        }
    }

}
