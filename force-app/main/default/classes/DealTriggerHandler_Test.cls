@isTest
public class DealTriggerHandler_Test {

    static final Id BROKER_TYPE = Schema.SObjectType.Contact.getRecordTypeInfosByName()
                                                        .get('Broker').getRecordTypeId();
    static final Id PROP_OWNER_TYPE = Schema.SObjectType.Contact.getRecordTypeInfosByName()
                                                        .get('Property Owner').getRecordTypeId();
    
    static final Id LEASE_TYPE = Schema.SObjectType.Deal__c.getRecordTypeInfosByName()
                                                        .get('Lease').getRecordTypeId();
    static final Id SALE_TYPE = Schema.SObjectType.Deal__c.getRecordTypeInfosByName()
                                                        .get('Sale').getRecordTypeId();

    static final String DEFAULT_STATUS = 'Open';

    static final String FIELDS = 'Id, Customer__c, Selling_Property__c, Saler__c, Status__c' 
                                + ', RecordTypeId, Lease_Month__c, Lease_End_Date__c';
    @testSetup 
    static void setup() 
    {      
        Contact owner1 = TestDataFactory.createContact('Test Contact', 'Owner 1', '', 'testowner1@gmail.com', PROP_OWNER_TYPE);
        Contact owner2 = TestDataFactory.createContact('Test Contact', 'Owner 2', '', 'testowner2@gmail.com', PROP_OWNER_TYPE);

        Property__c prop1 = TestDataFactory.createProperty(owner1, 5000, 400, 'Prop address 1');

        Deal__c lease1 = TestDataFactory.createDeal(owner1, owner2, prop1, '', LEASE_TYPE, '2022-1-15', '2022-9-14');
        Deal__c sale1 = TestDataFactory.createDeal(owner1, owner2, prop1, '', SALE_TYPE, '', '');
        Deal__c sale2 = TestDataFactory.createDeal(owner1, owner2, prop1, 'Awaiting Approval', SALE_TYPE, '', '');
    }

    @isTest
    static void testUpdateTrigger(){
        Deal__c dealForUpdate = Database.query('SELECT ' + FIELDS + 
                                                ' FROM Deal__c WHERE RecordTypeId = :SALE_TYPE LIMIT 1');
        DealManager.DealWrapper wrap = new DealManager.DealWrapper();
        wrap.Id = dealForUpdate.Id;
        wrap.saler = dealForUpdate.Saler__c;
        wrap.sellingProperty = dealForUpdate.Selling_Property__c;
        wrap.customer = dealForUpdate.Customer__c;
        wrap.status = 'Closed Won';

        Deal__c resultDeal = DealManager.updateDealsById(wrap, false);
        
        Test.startTest();
        Database.SaveResult result = Database.update(resultDeal);
        Test.stopTest();
        System.assert(result.isSuccess());

        List<Deal__c> deals = [SELECT Id, Status__c FROM Deal__c WHERE Selling_Property__c = :dealForUpdate.Selling_Property__c
                        AND RecordTypeId = :SALE_TYPE AND Status__c NOT IN ('Closed Won', 'Closed')];
            for (Deal__c deal : deals)
            {
                if (deal.Id == result.Id) continue;
                System.assertEquals('Closed Lost', deal.Status__c);
            }
    }
}
