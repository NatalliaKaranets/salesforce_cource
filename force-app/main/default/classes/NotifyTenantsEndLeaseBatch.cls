public class NotifyTenantsEndLeaseBatch implements Database.Batchable<sObject>{
    
    private static final String FIELDS = 'Id, CreatedDate'
    + ', Client__c.Id'
    + ', Client__c.FirstName'
    + ', Client__c.LastName'
    + ', Client__c.Email'
    + ', Selling_Property__c.Id'
    + ', Selling_Property__c.Property_Owner__r.Id'
    + ', Selling_Property__c.Property_Owner__r.FirstName'
    + ', Selling_Property__c.Property_Owner__r.LastName'
    + ', Selling_Property__c.Property_Owner__r.Email' 
    + ', Selling_Property__c.Country__c'
    + ', Selling_Property__c.City__c'
    + ', Selling_Property__c.Address__c'
    + ', Selling_Property__c.Latitude__c' 
    + ', Selling_Property__c.Longitude__c' 
    + ', Lease_End_Date__c'
    + ', Lease_Start_Date__c';
       
    public Database.QueryLocator start(Database.BatchableContext bc) {
        Date endLeaseDate = date.today().addDays(1);
        return Database.getQueryLocator('SELECT  + FIELDS + FROM Deal__c WHERE Lease_End_Date__c = :endLeaseDate');
    }

    public void execute(Database.BatchableContext bc, List<Deal__c> leases){
               List<NotifyClientRentEnding__e> rentEndingEventList = new List<NotifyClientRentEnding__e>();
        for (Deal__c deal : leases) {
            NotifyClientRentEnding__e rentEndingEvent = new NotifyClientRentEnding__e(
                DealId__c = deal.Id,
                Info__c = JSON.serialize(new GetDataByLease.Info(deal), true)
            );
            rentEndingEventList.add(rentEndingEvent);
        }

        SendEmailRentEnd.sendEmailsRemindEndRent(leases);
        List<Database.SaveResult> results = EventBus.publish(rentEndingEventList);
        for (Database.SaveResult sr : results) {
           if (sr.isSuccess()) {
                System.debug('Successfully published event.');
                  } else {
        for(Database.Error err : sr.getErrors()) {
            System.debug('Error returned: ' +
                        err.getStatusCode() +
                        ' - ' +
                        err.getMessage());
            }
          }
        }}

    public void finish(Database.BatchableContext bc){
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors,
            JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob
            WHERE Id = :bc.getJobId()];
        
        Log__c batchLog = new Log__c (
            Type__c = 'Batch Errors',
            Message__c = String.valueOf(job.NumberOfErrors)
        );
        insert batchLog;
    }
 }