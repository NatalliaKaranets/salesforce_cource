@IsTest

public class ContactManager_Test {
    
     @IsTest static void getContactsbyIdTest(){      
        Contact con = new Contact();       
        list<Id> ids = new list<Id>();
        ids.add('0035i00000EMMB5AAP');
        list<Contact> conTest = ContactManager.getContactsByIds(ids, 'Name');
        System.debug(conTest);
   
    }
    
     @IsTest static void createContactTest(){
        ContactManager.ContactWrapper contact = new ContactManager.ContactWrapper();
    	contact.FirstName = 'jshbfj';
    	contact.LastName = 'kjhbfvkjd';
        Contact con = ContactManager.createContact(contact, true);
     }
    
    @IsTest static void deleteContactsByEmailsTest() {
		list<String> emails = new list<String>();
        emails.add('Test@test.by');
        ContactManager.deleteContactsByEmails(emails);
     }
      
}