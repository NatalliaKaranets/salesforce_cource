@isTest
public class SendingTenantNoticeSchedule_Test {
      
    static final Id PROP_OWNER_TYPE = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Property Owner').getRecordTypeId();

    static final Id LEASE_TYPE = Schema.SObjectType.Deal__c.getRecordTypeInfosByName()
                                                    .get('Lease').getRecordTypeId();
    public static String CRON_EXP = '0 0 6 * * ?';

    @isTest static void testScheduledJob() {
        Test.startTest();
        String jobID = System.schedule('Notifying tenants of the end of the lease term.', CRON_EXP, new SendingTenantNotice());
        Test.stopTest();
        CronTrigger ct = [SELECT TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobID];

        if (Datetime.now().time() > Time.newInstance(6, 0, 0, 0)) {
            System.assertEquals(Datetime.newInstance(Datetime.now().addDays(1).date(), Time.newInstance(6, 0, 0, 0)), 
                                                            ct.NextFireTime);
        }
        else System.assertEquals(Datetime.newInstance(Datetime.now().addDays(1).date(), Time.newInstance(6, 0, 0, 0)), 
                                                            ct.NextFireTime);
    }
}
