@RestResource(urlMapping='/propery/v1/*')
global class RESTServiceProperty {

    @HttpGet
    global static String getProperty() {
        String response = '';
        String path = RestContext.request.requestURI.substringAfterLast('v1/');
        if (path.contains('by_owner?owner=')) {
            response = getPropertyByOwner(path.substringAfterLast('by_owner?owner=')); 
        }
        else if (path.contains('report/')) {
            response = getPropertyReport(path.substringAfterLast('report/').substringBefore('?'),
                                            path.substringAfterLast('startDate=').substringBefore('&'),
                                            path.substringAfterLast('endDate=').substringBefore('&'),
                                            path.substringAfterLast('dealType=')); 
        }
        else response = 'Error';

        return response;
    }

    @HttpPost
    global static String createProperty() {
        String response = '';
        GetDataByLease.PropertyPostWrapper wrapPost = (GetDataByLease.PropertyPostWrapper) 
                                                                JSON.deserialize(RestContext.request.requestBody.toString(), 
                                                                GetDataByLease.PropertyPostWrapper.class);
        PropertyManager.PropertyWrapper wrap = new PropertyManager.PropertyWrapper();
        wrap.address = wrapPost.address;
        wrap.city = wrapPost.city;
        wrap.country = wrapPost.country;
        wrap.propertyOwner = Id.valueOf(wrapPost.propertyOwner);
        wrap.rentCost = Decimal.valueOf(wrapPost.rentCost);
        wrap.sellingPrice = Decimal.valueOf(wrapPost.sellingPrice);
        if (wrapPost.latitude != null) wrap.latitude = Decimal.valueOf(wrapPost.latitude);
        if (wrapPost.longitude != null) wrap.longitude = Decimal.valueOf(wrapPost.longitude);
        
        Property__c result = PropertyManager.createProperty(wrap, true);
        response = JSON.serialize(result);
        return response;
    }

     @HttpPut
    global static String upsertPropertyOwner() {
        GetDataByLease.UpdatePropertyOwnerPUTWrapper wrapPut = (GetDataByLease.UpdatePropertyOwnerPUTWrapper)
                                                                      JSON.deserialize(RestContext.request.requestBody.toString(),
                                                                      GetDataByLease.UpdatePropertyOwnerPUTWrapper.class);
        Id ownerID = [SELECT Property_Owner__c FROM Property__c WHERE Id = :wrapPut.propertyId].Property_Owner__c;
        Contact owner = [SELECT Id, FirstName, LastName, Email FROM Contact WHERE Id =:ownerID LIMIT 1];
        owner.Id = String.isNotEmpty(wrapPut.newOwner.sfId) ? wrapPut.newOwner.sfId : owner.Id;
        owner.FirstName = String.isNotEmpty(wrapPut.newOwner.firstName) ? wrapPut.newOwner.firstName : owner.FirstName;
        owner.LastName = String.isNotEmpty(wrapPut.newOwner.lastName) ? wrapPut.newOwner.lastName : owner.LastName;
        owner.Email = wrapPut.newOwner.email;
        upsert owner;
        return JSON.serialize(owner);
    }


    private static String getPropertyByOwner(Id ownerId) {
        String fields = 'Id, Country__c, City__c, Address__c, Latitude__c, Longitude__c';
        List<Property__c> properties = PropertyManager.getPropertiesByOwners(new List<Id>{ownerId}, fields);
        List<GetDataByLease.PropertyGETWrapper> props = new List<GetDataByLease.PropertyGETWrapper>();
        for (Property__c prop : properties) {
            GetDataByLease.PropertyGETWrapper result = new GetDataByLease.PropertyGETWrapper(prop);
            props.add(result);
        }

        return JSON.serialize(props, true);
    }

    private static String getPropertyReport(String propId, String startDate, String endDate, String recTypeName) {
        String fields = 'Id, RecordType.DeveloperName, Rent_Cost__c' 
                                            + ', Customer__r.Id'
                                            + ', Saler__r.Id'
                                            + ', Saler__r.FirstName'
                                            + ', Saler__r.LastName'
                                            + ', Saler__r.Email'
                                            + ', Selling_Property__r.Id';

        Date startPeriod = Date.valueOf(startDate);
        Date endPeriod = Date.valueOf(endDate);
        List<Deal__c> deals = DealManager.getDealsByPeriodAndType(propId, startPeriod, endPeriod, recTypeName, fields);
        GetDataByLease.Response response = new GetDataByLease.Response(startPeriod, endPeriod, deals);

        return JSON.serialize(response, true);
    }
}