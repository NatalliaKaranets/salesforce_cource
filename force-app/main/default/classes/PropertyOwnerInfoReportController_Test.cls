@isTest
public class PropertyOwnerInfoReportController_Test {

    static final Id PROP_OWNER_TYPE = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Property Owner').getRecordTypeId();
    static final Id INDUSTRIAL_PROPERTY_TYPE = Schema.SObjectType.Property__c.getRecordTypeInfosByName().get('Industrial').getRecordTypeId();
    static final Id OFFICE_PROPERTY_TYPE = Schema.SObjectType.Property__c.getRecordTypeInfosByName().get('Office').getRecordTypeId();
    static final Id SPACE_PROPERTY_TYPE = Schema.SObjectType.Property__c.getRecordTypeInfosByName().get('Space').getRecordTypeId();
    static final Id SALE_TYPE = Schema.SObjectType.Deal__c.getRecordTypeInfosByName().get('Sale').getRecordTypeId();

    @testSetup 
    static void setup() 
    {       
        Contact owner1 = TestDataFactory.createContact('FirstName1', 'Owner 1', '123456789', 'owner1@test.con', PROP_OWNER_TYPE);
        Contact owner2 = TestDataFactory.createContact('FirstName2', 'Owner 2', '987654321', 'owner2@test.con', PROP_OWNER_TYPE);

        Property__c prop1 = TestDataFactory.createProperty(owner1, 1000000, 200,  'Prop address 1', INDUSTRIAL_PROPERTY_TYPE);
        Property__c prop2 = TestDataFactory.createProperty(owner2, 600000, 120, 'Prop address 2', INDUSTRIAL_PROPERTY_TYPE);
        Property__c prop3= TestDataFactory.createProperty(owner2, 1500000, 500, 'Prop address 3', OFFICE_PROPERTY_TYPE);
        Property__c prop4 = TestDataFactory.createProperty(owner1, 400000, 200, 'Prop address 4', OFFICE_PROPERTY_TYPE);
        Property__c prop5 = TestDataFactory.createProperty(owner1, 1200000, 600, 'Prop address 5', SPACE_PROPERTY_TYPE);
        Property__c prop6 = TestDataFactory.createProperty(owner2, 900000, 300, 'Prop address 6', SPACE_PROPERTY_TYPE);
        
        Deal__c sale1 = TestDataFactory.createDeal(owner1, owner2, prop1, '', SALE_TYPE, '', '');
        Deal__c sale2 = TestDataFactory.createDeal(owner1, owner2, prop2, 'Closed Won', SALE_TYPE, '', '');
        Deal__c sale3 = TestDataFactory.createDeal(owner1, owner2, prop3, '', SALE_TYPE, '', '');
        Deal__c sale4 = TestDataFactory.createDeal(owner1, owner2, prop4, 'Closed Won', SALE_TYPE, '', '');
        Deal__c sale5 = TestDataFactory.createDeal(owner1, owner2, prop5, 'Closed Won', SALE_TYPE, '', '');
        Deal__c sale6 = TestDataFactory.createDeal(owner1, owner2, prop6, 'Closed Won', SALE_TYPE, '', '');
            }

    @isTest static void testgetReportData() {
        Contact propertyOwner = [SELECT Id, Name FROM Contact WHERE LastName = 'Owner 1' LIMIT 1];
       	ApexPages.StandardController stdController = new ApexPages.StandardController(propertyOwner);
        PropertyOwnerInfoReportController controller = new PropertyOwnerInfoReportController(stdController);

        List<PropertyOwnerInfoReportController.ReportDataWrapper> result = controller.getReportData();
        System.assert(true);
        System.assert(result.size() == 1);
        System.assertEquals(1000000, result[1].dealsAmount);
        System.assertEquals(100, result[1].dealsAmoutWithoutCommission);
    }
}
