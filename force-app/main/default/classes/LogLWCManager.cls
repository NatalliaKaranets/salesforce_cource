public inherited sharing class LogLWCManager {

    public static List<LogLWC__c> getLogsLWC(string fields) {
        return Database.query('SELECT ' + fields + ' FROM LogLWC__c');
    }

    public static List<LogLWC__c> getLogsLWCByObjectType(string fields, string objectType) {
        return Database.query('SELECT ' + fields + ' FROM LogLWC__c WHERE ObjectType__c = :objectType');
    }

    public static LogLWC__c createLogLWC(LogLWCController.LogLWCWrapper wrapper, Boolean serialize) {
        LogLWC__c logLWC = new LogLWC__c();
        logLWC.ObjectType__c = wrapper.objectType;
        logLWC.ActionType__c = wrapper.actionType;
        logLWC.Description__c = wrapper.description;
        logLWC.IsSuccessful__c = wrapper.isSuccessful;
        logLWC.ErrorMessage__c = wrapper.errorMessage;
        if (serialize) insert logLWC;
        return logLWC;
    }
}