public with sharing class LogLWCController {

    public class LogLWCWrapper{
        @AuraEnabled
        public String objectType {get; set;}
        @AuraEnabled
        public String actionType {get; set;}
        @AuraEnabled
        public String description {get; set;}
        @AuraEnabled
        public Boolean isSuccessful {get; set;}
        @AuraEnabled
        public String errorMessage {get; set;}
    }

        @AuraEnabled(cacheable=false)
        public static LogLWC__c createLogLWC(String objectType, String actionType, String description, String errorMessage){
            System.debug('objectType ==> ' + objectType);
            try {
                LogLWCManager.LogLWCWrapper wrapper = new LogLWCManager.LogLWCWrapper();
                wrapper.objectType = objectType;
                wrapper.actionType = actionType;
                wrapper.description = description;
                wrapper.isSuccessful = String.isBlank(errorMessage) ? true : false;
                wrapper.errorMessage = errorMessage;    
    
                return LogLWCManager.createLogLWC(wrapper, true);
            } catch (Exception e) {
                throw new AuraHandledException(e.getMessage());
            }
        }
    
        @AuraEnabled(cacheable=true)
        public static List<LogLWC__c> getLogsLWC(String fields){
            try {
                return LogLWCManager.getLogsLWC(fields);
            } catch (Exception e) {
                throw new AuraHandledException(e.getMessage());
            }
        }
}