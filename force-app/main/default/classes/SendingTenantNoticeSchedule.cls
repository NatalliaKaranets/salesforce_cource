public class SendingTenantNoticeSchedule implements Schedulable {
    public void execute(SchedulableContext ctx) {
        Database.executeBatch(new NotifyTenantsEndLeaseBatch());
    }
}
