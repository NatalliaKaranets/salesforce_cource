public inherited sharing class LogManager {

    public static final String DEFAULT_TYPE = 'DEBUG';

    public class LogWrapper {
        public String Type {get; set;}
        public String message {get; set;}
    }

    public static List<Log__c> getLogsByIds(List<Id> ids, String fields) {
         return Database.query('SELECT ' + fields + ' FROM Log__c WHERE Id IN :ids');
    }

    public static List<Log__c> getLogsByTypes(List<String> types, String fields) {
        return Database.query('SELECT ' + fields + ' FROM Log__c WHERE Type__c IN :types');
    }

    public static Log__c createLog(String type, String message) {

        Log__c createdLog = new Log__c();

        createdLog.Type__c = type;
        createdLog.Message__c = message;

        insert createdLog;
        
        return createdLog;
    }

    public static Log__c updateLog(String id, LogWrapper newDeal, Boolean serialize) {
		try {
			Log__c queriedLog = [SELECT Type__c, Message__c  FROM Log__c WHERE Id = id];

			queriedLog.Type__c = newLog.Type;
			queriedLog.Message__c = newLog.message;
			update queriedLog;
            return queriedLog;
        } catch(Exception e) {
            System.debug('An unexpected error has occurred: ' + e.getMessage());
            return null;
        }
    }

     public static void deleteLogsByIds(List<String> types) {
        try {
            delete [SELECT Id FROM Log__c WHERE Type__c IN :types];
        } catch (Exception e) {
            System.debug('Delete Log__c error: ' + e.getMessage());
        }
    }
}