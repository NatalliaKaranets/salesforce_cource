@isTest
public with sharing class LogLWCManager_Test {

    static final String FIELDS = 'ObjectType__c, ActionType__c, Description__c, IsSuccessful__c, ErrorMessage__c, CreatedDate';

     @testSetup 
    static void setup() 
    {      
        LogLWC__c log1 = TestDataFactory.createLogLWC('Property__c', 'Insert', '1 record was insered in the record type "Indusrtial"', true, '');
        LogLWC__c log2 = TestDataFactory.createLogLWC('Property__c', 'Update', '1 record was updated in the record type "Industrial"', false, 'an error occurred while updating');
        LogLWC__c log3 = TestDataFactory.createLogLWC('Property__c', 'Delete', '1 records was deleted in the record type "Industrial"', false, 'Error record cannot be deleted');
    }
    
    @isTest static void createLogLWC(){
        LogLWCManager.LogLWCWrapper wrapper = new LogLWCManager.LogLWCWrapper();
        wrapper.objectType = 'Property__c';
        wrapper.actionType = 'Insert';
        wrapper.description = '1 record was insered in the record type "Indusrtial"';
        wrapper.isSuccessful = true;
        wrapper.errorMessage = '';

        LogLWC__c logLWC = LogLWCManager.createLogLWC(wrapper, false);
        System.assertEquals(true, logLWC.IsSuccessful__c);
    }

    @isTest static void testGetLogsLWCByObjectType(){
        List<LogLWC__c> LogsLWCList = LogLWCManager.getLogsLWCByObjectType(FIELDS, 'Deal__c');
        System.assertEquals(1, LogsLWCList.size());
    }

    @isTest static void testGetLogsLWC(){
        List<LogLWC__c> logsLWCListTest = [SELECT Id FROM LogLWC__c];
        List<LogLWC__c> LogsLWCList = LogLWCManager.getLogsLWC(FIELDS);
        System.assertEquals(logsLWCListTest.size(), LogsLWCList.size());
    }
}
