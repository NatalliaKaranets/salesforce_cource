public inherited sharing class ContactManager {

    public static final Id DEFAULT_RECORD_TYPE = Schema.SObjectType.Contact.getRecordTypeInfosByName()
    	.get('Property Owner').getRecordTypeId();

	public class ContactWrapper {
		public String FirstName {get; set;}
		public String LastName {get; set;}
		public String Phone {get; set;}
		public String HomePhone {get; set;}
		public String Email {get; set;}
		public Id RecordTypeId {get; set;}
	}

	public static List<Contact> getContactsByIds(List<Id> ids, String fields) {
		return Database.query('SELECT ' + fields + ' FROM Contact WHERE Id IN :ids');
	}

	public static List<Contact> getContactsByRecordTypes(List<Id> recordTypes, String fields) {
		return Database.query('SELECT ' + fields + ' FROM Contact WHERE RecordTypeId IN :recordTypes');
	}

	public static Contact createContact(ContactWrapper wrapper, Boolean serialize) {
		Contact contact = new Contact();
		contact.FirstName = wrapper.FirstName;
		contact.LastName = wrapper.LastName;
		contact.Phone = wrapper.Phone;
		contact.HomePhone = wrapper.HomePhone;
		contact.Email = wrapper.email;
		contact.RecordTypeId = String.isNotBlank(wrapper.recordTypeId) ? wrapper.recordTypeId : DEFAULT_RECORD_TYPE;

		if (serialize) insert contact;

		return contact;
	}

	 public static List<Contact> updateContactsEmailByIds(String newEmail, List<Id> ids, Boolean serialize) {
        List<Contact> contactsToUpdate = [SELECT Email FROM Contact WHERE Id IN :ids];
        for(Contact contact : contactsToUpdate) {
            contact.Email = newEmail;
        }

        if (serialize) update contactsToUpdate;

        return contactsToUpdate;
    }

	 public static void deleteContactsByEmails(List<String> emails) {
        delete [SELECT Id FROM Contact WHERE Email IN :emails];
    }
}