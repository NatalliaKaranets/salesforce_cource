@IsTest
public class NotifyTenantsEndLeaseBatch_Test {
    
    static final Id PROP_OWNER_TYPE = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Property Owner').getRecordTypeId();

    static final Id LEASE_TYPE = Schema.SObjectType.Deal__c.getRecordTypeInfosByName().get('Lease').getRecordTypeId();
    
    @TestSetup static void setup() {
        Contact client = TestDataFactory.createContact('FirstNameClient', 'LastNameClient', '123456789', 'client@gmail.com', 'id');
        Contact owner = TestDataFactory.createContact('FirstNameOwner', 'LastNameOwner', '987654321', 'Owner@gmail.com', 'id');
        client.Email = 'Test@gmail.com';
        update client;
        Property__c property = TestDataFactory.createProperty(owner, 10000.0, 50.0, 'address');
        TestDataFactory.createDeal(owner, client, property, 'open', 'lease', '15.10.2021', '14.10.2022');
    }

    @IsTest static void testDealEndRentBatch() {
        Test.startTest();
        Id batchId = Database.executeBatch(new NotifyTenantsEndLeaseBatch());
        Test.stopTest();

    }

    @IsTest static void testEmailSending() {
        Test.startTest();
        Id batchId = Database.executeBatch(new NotifyTenantsEndLeaseBatch());
        Test.stopTest();
        System.assertEquals(0, [SELECT COUNT() FROM Log__c WHERE Type__c = 'sendEmailRentEnd error']);
    }
}
