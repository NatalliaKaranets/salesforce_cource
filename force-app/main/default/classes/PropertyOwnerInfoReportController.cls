public class PropertyOwnerInfoReportController {
    private final Contact contact;
    private static final String DEAL_STATUS_CLOSED_WON = 'Closed Won';
    private static final Set<String> FIELDS_FOR_DEAL_QUERY = new Set<String>{
            'Id', 'Deal_Amount__c', 'Status__c', 'Sum_After_Commissions__c',
            'Selling_Property__r.RecordTypeId', 'Selling_Property__r.RecordType.Name'
    };
    
    public PropertyOwnerInfoReportController(ApexPages.StandardController stdController) {
        this.contact = (Contact)stdController.getRecord();
    }

    public class ReportDataWrapper {
        public String propertyType { get; set; }
        public Integer numberDeals { get; set; }
        public Decimal dealsAmount { get; set; }
        public Decimal dealsAmoutWithoutCommission { get; set; }
        public ReportDataWrapper(String propertyType, Integer numberDeals, Decimal dealsAmount, Decimal dealsAmoutWithoutCommission) {
            this.propertyType = propertyType;
            this.numberDeals = numberDeals;
            this.dealsAmount = dealsAmount;
            this.dealsAmoutWithoutCommission = dealsAmoutWithoutCommission;
        }
    }
    
    public List<ReportDataWrapper> getReportData() {
        List<Deal__c> salerDeals = DealManager.getDealsBySalerId(contact.Id, FIELDS_FOR_DEAL_QUERY);
        Map<Id, ReportDataWrapper> dataWrapperByPropertyType = new Map<Id, ReportDataWrapper>();
        
        for (Deal__c deal : salerDeals) {
            if (deal.Status__c != DEAL_STATUS_CLOSED_WON) {
                continue;
            }
            String propertyRecordTypeId = deal.Selling_Property__r.RecordTypeId;
            
            if (!dataWrapperByPropertyType.keySet().contains(propertyRecordTypeId)) {
                ReportDataWrapper dataWrapper = new ReportDataWrapper(deal.Selling_Property__r.RecordType.Name, 1, deal.Deal_Amount__c, deal.Sum_After_Commissions__c);
                dataWrapperByPropertyType.put(propertyRecordTypeId, dataWrapper);
            } else {
                dataWrapperByPropertyType.get(propertyRecordTypeId).numberDeals += 1;
                dataWrapperByPropertyType.get(propertyRecordTypeId).dealsAmount += deal.Deal_Amount__c;
                dataWrapperByPropertyType.get(propertyRecordTypeId).dealsAmoutWithoutCommission += deal.Sum_After_Commissions__c;    
            }
        }
        
        return dataWrapperByPropertyType.values();
    }
}
