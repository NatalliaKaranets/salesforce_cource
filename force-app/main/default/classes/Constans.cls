public class Constants {
    public static final String DEAL_STATUS_WON = 'Closed Won';
    public static final String DEAL_STATUS_LOST = 'Closed Lost';
    public static final String RECORD_TYPE_NAME_SALE = 'Sale';
}