public class DealTriggerHandler extends TriggerHandler {

    Map<Id, Deal__c> newDealMap;

    public DealTriggerHandler() {
        this.newDealMap = (Map<Id, Deal__c>) Trigger.newMap;
        this.oldDealMap = (Map<Id, Deal__c>) Trigger.oldMap;
    }
    
    public override void afterUpdate () {
        List<Deal__c> needUpdateDeals = new List<Deal__c>();
        List<Property__c> properties = new List<Property__c>();
        Set<Id> propertyIds = new Set<Id>();
        
        Id saleRecordTypeId = Schema.SObjectType.Deal__c.getRecordTypeInfosByName().get(Constants.RECORD_TYPE_NAME_SALE).getRecordTypeId();

        for (Deal__c deal : newDealMap.values()) {
            if (deal.Status__c != Trigger.oldMap.get(deal)) {
            if (deal.Status__c == Constants.DEAL_STATUS_WON && deal.RecordTypeId == saleRecordTypeId) {
                propertyIds.add(deal.Selling_Property__c);
            }
        }

        properties = [
            SELECT (SELECT Id, Status__c FROM Deals__r WHERE Status__c != :Constants.DEAL_STATUS_WON AND Status__c != :Constants.DEAL_STATUS_LOST)
            FROM Property__c
            WHERE Id IN :propertyIds
        ];
     
        for (Property__c property : properties) {
            for (Deal__c dealToUpdate : property.Deals__r) {
                dealToUpdate.Status__c = Constants.DEAL_STATUS_LOST;
                needUpdateDeals.add(dealToUpdate);
            }
        }
        
        update needUpdateDeals;
    }
}