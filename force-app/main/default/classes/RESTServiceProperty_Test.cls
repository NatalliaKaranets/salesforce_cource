@isTest
public class RESTServiceProperty_Test {
    
    static final Id SALER_TYPE = Schema.SObjectType.Contact.getRecordTypeInfosByName()
                                                        .get('Saler').getRecordTypeId();
    static final Id CUSTOMER_TYPE = Schema.SObjectType.Contact.getRecordTypeInfosByName()
                                                        .get('Customer').getRecordTypeId();
     static final Id PROP_OWNER_TYPE = Schema.SObjectType.Contact.getRecordTypeInfosByName()
                                                        .get('Property Owner').getRecordTypeId();
     static final Id LEASE_TYPE = Schema.SObjectType.Deal__c.getRecordTypeInfosByName()
                                                        .get('Lease').getRecordTypeId();
    static final Id SALE_TYPE = Schema.SObjectType.Deal__c.getRecordTypeInfosByName()
                                                        .get('Sale').getRecordTypeId();
    
    
     @testSetup
    static void setup()
    {
        Contact saler = TestDataFactory.createContact('Test Saler', 'Saler', '123456789', 'testseler@gmail.com', SALER_TYPE);
        Contact customer = TestDataFactory.createContact('Test Customer', 'Customer', '987654321', 'testcustomer@gmail.com', CUSTOMER_TYPE);
        Contact owner1 = TestDataFactory.createContact('Test Owner', 'Owner 1', '675849302', 'testowner@gmail.com', PROP_OWNER_TYPE);
        Contact owner2 = TestDataFactory.createContact('Test Contact', 'Owner 2', '', 'testowner2@gmail.com', PROP_OWNER_TYPE);
        Property__c prop1 = TestDataFactory.createProperty(owner1, 5000, 400, 'Prop address 1');
        Property__c prop2 = TestDataFactory.createProperty(owner2, 1000, 300, 'Prop address 2');
        Deal__c lease1 = TestDataFactory.createDeal(owner1, owner2, prop1, '', LEASE_TYPE, '2022-1-15', '2022-9-14');
        Deal__c sale1 = TestDataFactory.createDeal(owner1, owner2, prop1, '', SALE_TYPE, '', '');
        Deal__c sale2 = TestDataFactory.createDeal(owner1, owner2, prop1, '', SALE_TYPE, '', '');
       
    }

    @isTest static void getPropertyByOwnerTest() {
        Contact owner = [SELECT Id, LastName FROM Contact WHERE Email = 'test@mail.ru' LIMIT 1];
        RestRequest request = new RestRequest();
        request.requestUri = 'https://senla14-dev-ed.my.salesforce.com/services/data/v55.0/sobjects/propery/v1/by_owner?owner=' + owner.Id;
        request.httpMethod = 'GET';
        RestContext.request = request;

        String result = RESTServiceProperty.getProperty();
        System.assert(result != null);
    }

    @isTest static void getPropertyReportTest() {
        Property__c prop = [SELECT Id FROM Property__c WHERE Rent_Cost__c = 10000 LIMIT 1];
        System.debug(prop);
        RestRequest request = new RestRequest();
        request.requestUri = 'https://senla14-dev-ed.my.salesforce.com/services/data/v55.0/sobjects/propery/v1/report/' + prop.Id +'?startDate=2022-09-01&endDate=2022-10-01&dealType=<Lease>';
        request.httpMethod = 'GET';
        RestContext.request = request;

        String result = RESTServiceProperty.getProperty();
        System.debug(result);
        System.assert(result != null);
    }

    @isTest static void createPropertyTest() {
        Contact owner = [SELECT Id FROM Contact WHERE Email = 'test@tut.com' LIMIT 1];
        GetDataByLease.PropertyPOSTWrapper wrap = new GetDataByLease.PropertyPOSTWrapper();
        wrap.country = 'USA';
        wrap.city = 'Chicago';
        wrap.address = '671 Lincoln Ave';
        wrap.propertyOwner = String.valueOf(owner.Id);
        wrap.rentCost = '10000';
        wrap.sellingPrice = '10000000';

        RestRequest request = new RestRequest();
        request.requestUri = 'https://senla14-dev-ed.my.salesforce.com/services/data/v55.0/sobjects/propery/v1/add';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrap));
        RestContext.request = request;
        
        String result = RESTServiceProperty.createProperty();
        
        System.assert(result != null);
        System.assert(([SELECT Id FROM Property__c WHERE Country__c = 'Canada']).size() > 0);
    }

    @isTest static void upsertPropertyOwnerTest() {
        Property__c prop = [SELECT Id FROM Property__c LIMIT 1];
        GetDataByLease.UpdatePropertyOwnerPUTWrapper wrap = new GetDataByLease.UpdatePropertyOwnerPUTWrapper();
        wrap.propertyId = prop.Id;
        wrap.newOwner = new GetDataByLease.Client();
        wrap.newOwner.firstName = 'testName';
        wrap.newOwner.email = 'newEmail@mail.ru';

        RestRequest request = new RestRequest();
        request.requestUri = 'https://senla14-dev-ed.my.salesforce.com/services/data/v55.0/sobjects/propery/v1';
        request.httpMethod = 'PUT';
        request.requestBody = Blob.valueOf(JSON.serialize(wrap));
        RestContext.request = request;

        String result = RESTServiceProperty.upsertPropertyOwner();

        System.assert(result != null);
        System.assert([SELECT COUNT() FROM Property__c WHERE Property_Owner__r.Email = 'PO@gmail.com'] > 0);
    }
}
