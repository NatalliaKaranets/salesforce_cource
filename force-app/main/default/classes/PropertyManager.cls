public inherited sharing class PropertyManager {

    public static final Id DEFAULT_RECORD_TYPE = Schema.SObjectType.Property__c.getRecordTypeInfosByName()
    	.get('Industrial').getRecordTypeId();

	public class PropertyWrapper {
		public String Name {get; set;}
		public String Address {get; set;}
		public String country {get; set;}
        public String city {get; set;}
        public Decimal latitude {get; set;}
        public Decimal longitude {get; set;}
		public Decimal SellingPrice {get; set;}
		public Decimal RentCost {get; set;}
        public String PropertyOwner {get; set;}
		public Id RecordTypeId {get; set;}
	}

	public static List<Property__c> getPropertysByIds(List<Id> ids, String fields) {
		return Database.query('SELECT ' + fields + ' FROM Property__c WHERE Id IN :ids');
	}

	public static List<Property__c> getPropertysByRecordTypes(List<String> Address, String fields) {
		return Database.query('SELECT ' + fields + ' FROM Property__c WHERE Address__c IN :address');
	}
	
	public static List<Property__c> getPropertiesByOwners(List<Id> ownersIds, String fields) {
        return Database.query('SELECT ' + fields + ' FROM Property__c WHERE Property_Owner__c IN :ownersIds');
    }

	public static Property__c createProperty(PropertyWrapper wrapper, Boolean serialize) {
		Property__c property = new Property__c();
		property.Address__c = wrapper.address;
		property.Selling_price__c = wrapper.sellingPrice;
		property.Rent_Cost__c = wrapper.rentCost;
		property.Property_Owner__c = wrapper.PropertyOwner;
		property.RecordTypeId = String.isNotBlank(wrapper.recordTypeId) ? wrapper.recordTypeId : DEFAULT_RECORD_TYPE;

		if (serialize) insert property;

		return property;
	}

	public static Property__c updateProperty(String id, PropertyWrapper newProperty, Boolean serialize) {
		try {
			Property__c queriedProperty = [SELECT Name, Address__c, Selling_Price__c, Rent_Cost__c, Property_Owner__c FROM Property__c WHERE ID=:id];

			queriedProperty.Address__c = newProperty.Address;
			queriedProperty.Selling_Price__c = newProperty.SellingPrice;
			queriedProperty.Rent_Cost__c = newProperty.RentCost;
			queriedProperty.Property_Owner__c = newProperty.PropertyOwner;
			update queriedProperty;
			return queriedProperty;
        } catch(Exception e) {
            System.debug('Apdate "Property__c" error: ' + e.getMessage());
			return null;
        }
    }
	
	public static void deletePropertiesByAddresses(List<String> addresses) {
        delete [SELECT Id FROM Property__c WHERE Address__c IN :addresses];
    }
}