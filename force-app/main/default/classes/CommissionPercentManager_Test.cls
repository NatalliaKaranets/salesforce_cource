@isTest
public class CommissionPercentManager_Test {
    @TestSetup static void setup() {
        Contact c1 = new Contact(LastName = 'seller');
        insert c1;
        Contact c2 = new Contact(LastName = 'customer');
        insert c2;
        Contact c3 = new Contact (LasrName = 'broker');
        insert c3;
        Contact broker = TestDataFactory.createContact(Broker, '1234567890');
        Property__c p = TestDataFactory.createProperty(c1, 10000, 50, 'test');
        Deal__c d = TestDataFactory.createDeal(c1, c2, p.Id);
        TestDataFactory.createCommissionPercent(d.Id, broker.Id, 10);
    }

    @IsTest static void testCreateCommissionPercent() {
        Id dealId = [SELECT Id FROM Deal__c LIMIT 1].Id;
        Id brokerId = [SELECT Id FROM Contact WHERE LastName = :BROKER].Id;
        Commission_Percent__c commissionPercent = CommissionPercentManager.createCommissionPercent(dealId, 10, brokerId, false);
        Test.startTest();
        Database.SaveResult result = Database.insert(commissionPercent);
        Test.stopTest();
        System.assert(result.isSuccess());
    }

    @IsTest static void testGetCommissionPercentsByIds() {
        Id commissionPercentId = [SELECT Id FROM Commission_Percent__c LIMIT 1].Id;
        System.assertEquals(1, CommissionPercentManager.getCommissionPercentsByIds(new List<Id>{commissionPercentId},'Id').size());
    }

    @IsTest static void testGetCommissionPercentsByDeals() {
        Id dealId = [SELECT Deal__c FROM Commission_Percent__c LIMIT 1].Deal__c;
        System.assertEquals(1, CommissionPercentManager.getCommissionPercentsByDeals(new List<Id>{dealId},'Id').size());
    }

    @IsTest static void testGetCommissionPercentsByBrokers() {
        Id brokerId = [SELECT Broker__c FROM Commission_Percent__c WHERE Commission_Percent__c = :10].Broker__c;
        System.assertEquals(1, CommissionPercentManager.getCommissionPercentsByBrokers(new List<Id>{brokerId},'Id').size());
    }

    @IsTest static void testDeleteCommissionPercentsByBrokers() {
        Id brokerId = [SELECT Id FROM Contact WHERE LastName = :BROKER LIMIT 1].Id;
        CommissionPercentManager.deleteCommissionPercentsByBrokers(new List<Id>{brokerId});
        System.assertEquals(0, [SELECT COUNT() FROM Commission_Percent__c WHERE Broker__c = :brokerId]);
    }

    @IsTest static void testUpdateCommissionPercentsByIds() {
        Id commissionPercentId = [SELECT Id FROM Commission_Percent__c LIMIT 1].Id;
        List<Commission_Percent__c> updatedList =
                CommissionPercentManager.updateCommissionPercentsByIds(new List<Id>{commissionPercentId}, 4, true);
        System.assertEquals(1, updatedList.size());
    }

    @IsTest static void testUpdateCommissionPercentsByBrokers() {
        Id brokerId = [SELECT Id FROM Contact WHERE LastName = :BROKER LIMIT 1].Id;
        CommissionPercentManager.updateCommissionPercentsByBrokers(new List<Id>{brokerId}, 4, true);
        System.assertEquals(1, [SELECT COUNT() FROM Commission_Percent__c WHERE Commission_Percent__c = :4]);
    }
}
