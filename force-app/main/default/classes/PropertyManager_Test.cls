@isTest
public with sharing class PropertyManager_Test {
    
     @IsTest static void getPropertysByIdsTest(){      
        list<Id> ids = new list<Id>();
        ids.add('a015i00000XIVupAAH');
        list<Property__c> ppList = PropertyManager.getPropertysByIds(ids, 'Number');
        System.debug(ppList);
   }

    @IsTest static void createPropertyTest(){
        PropertyManager.PropertyWrapper property = new PropertyManager.PropertyWrapper();
    	property.address = 'address test';
    	property.propertyOwner = 'test';
        Property__c pp = PropertyManager.createProperty(property, true);
    }
     
    @isTest static void testDeletePropertiesByAddresses(){
        List<String> addressesList = new List<String>();
        addressesList.add('Property address');

        PropertyManager.deletePropertiesByAddresses(addressesList);
        List<Property__c> resultDBProperties = [SELECT Id FROM Property__c WHERE Address__c = 'Property address'];
        System.assertEquals(0, resultDBProperties.size());
	}
}
