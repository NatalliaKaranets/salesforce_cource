import { LightningElement, wire, api } from 'lwc';
import { getRecord, getFieldValue } from "lightning/uiRecordApi";
import { NavigationMixin } from 'lightning/navigation';

const fields = [
  'Property__c.Property_Owner__r.Id',
  'Property__c.Property_Owner__r.FirstName',
  'Property__c.Property_Owner__r.LastName',
  'Property__c.Property_Owner__r.Phone',
  'Property__c.Property_Owner__r.HomePhone',
  'Property__c.Property_Owner__r.Email',
  'Property__c.Property_Owner__r.Total_Property_Price__c',
];

export default class ownerDetails extends NavigationMixin(LightningElement) {
    @api
  recordId;
  @wire(getRecord, {recordId: "$recordId", fields})
  owner;
 
  get data(){
    return [Array].map(() => {
      return {
          firstName: getFieldValue(this.owner.data, 'Property__c.Property_Owner__r.FirstName') ?? '',
          lastName: getFieldValue(this.owner.data, 'Property__c.Property_Owner__r.LastName') ?? '',
          phone: getFieldValue(this.owner.data, 'Property__c.Property_Owner__r.Phone') ?? '',
          homePhone: getFieldValue(this.owner.data, 'Property__c.Property_Owner__r.HomePhone') ?? '',
          email: getFieldValue(this.owner.data, 'Property__c.Property_Owner__r.Email') ?? '',
          totalPropertyPrice:getFieldValue(this.owner.data, 'Property__c.Property_Owner__r.Total_Property_Price__c') ?? ''
      };
  });
  }
  handleClick(event) {
    event.preventDefault();
    this[NavigationMixin.Navigate]({
      type: 'standard__recordPage',
      attributes: {
          recordId: getFieldValue(this.owner.data, 'Property__c.Property_Owner__r.Id'),
          objectApiName:'Contact__c',
          actionName: 'view'
      },
  });
  }
}