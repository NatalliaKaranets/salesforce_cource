import { LightningElement, api, wire, track } from 'lwc';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import PROPERTY_OBJECT from '@salesforce/schema/Property__c';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import {TOAST_EVENT_VARIANT_SUCCESS,
	TOAST_EVENT_VARIANT_ERROR,
	LOGLWC_ACTION_TYPE_INSERT,
	showToast} from 'c/constantLwcUtils'

export default class PropertyCreationChild extends LightningElement {
	isPropertyRecordType = true
	@api recordId;
	@track objectNameToGetRecordTypes = 'Property__c';
	@wire(getObjectInfo, { objectApiName: PROPERTY_OBJECT })
	objectInfo;
	@track propertyes = []
	@api currentRecordTypeId = '0125i000000tqJNAAY'
	@track forms = [1]

   
	connectedCallback() {
		const propertyes = this.objectInfo.data.recordTypeInfos;
		Object.keys(propertyes).forEach((element, index ) => {
			if(!propertyes[element].master) {
				this.propertyes.push({
					value: propertyes[element].name,
					recordTypeId: propertyes[element].recordTypeId,
					checked: propertyes[element].name === 'Industrial'
				})
				this.currentRecordTypeId = this.propertyes[0].recordTypeId

			}
		})
	}

	handleChangeRecordType(event){
		const valueRecordType = event.target.name 
		if (valueRecordType != this.currentRecordTypeId) {
			this.currentRecordTypeId = valueRecordType
			this.propertyes.forEach(property => {
				property.recordTypeId === valueRecordType ? property.checked = 1 : property.checked = 0
			})
		} else { 
			this.currentRecordTypeId = null
		}
	}

	onChangeTemplate () {
		this.isPropertyRecordType = this.isPropertyRecordType ? false : true
	}

	@track disableDelete = true
    @track disableAdd = false
    lastNumber = 1
	@api objectApiName
	recordTypeId
	propertyOwner = window.location.pathname.split('/')[4]

	handleSubmit(){
		try {
			this.template.querySelectorAll('lightning-record-edit-form').forEach(form => {
				form.submit()
			})
			this.showToast(TOAST_EVENT_VARIANT_SUCCESS, property_created,TOAST_EVENT_VARIANT_SUCCESS)
			this.createLog(LOGLWC_ACTION_TYPE_INSERT)
		} catch (error){
			this.showToast(record_created_error, error.body.message, TOAST_EVENT_VARIANT_ERROR)
			this.createLog(LOGLWC_ACTION_TYPE_INSERT, error.body.message)
		}
	}
		
	showToast(title, msg, variant){
		this.dispatchEvent(
			new ShowToastEvent({
				title: title,
				message: msg,
				variant: variant
			})
		)
		this.goToTypes()
	}

	@track forms = [1]

	handleAdd() {
		++this.lastNumber
		if (this.forms.length === 1){
			this.forms.push(this.lastNumber)
			this.disableDelete = false
		} else {
			this.forms.push(this.lastNumber)
			this.disableAdd = true
		}
	}
		
	handleDelete(event) {
		if (this.forms.length > 1){
			this.disableDelete = this.forms.length === 2
			this.disableAdd = this.forms.length === 3
			this.forms = this.forms.filter(item => {
				return item != parseInt(event.target.accessKey)
			})
		}
    }

	createLog(type, error) {
		let recordType = this.propertyes.filter(item => item.recordTypeId === this.currentRecordTypeId)
		createLogLWC({
			logObjectType: PROPERTY_OBJECT.objectApiName, 
			logActionType: type,
			logIsSuccessful: error ? 'false' : 'true',
			logDescription: '1 records created with' + recordType.name + 'recordType',
			logErrorMessage: error ? error : ''
		})	
    }
}
   
	   