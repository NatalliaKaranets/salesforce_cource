import { LightningElement, track, wire } from 'lwc';
import getLogsLWC from '@salesforce/apex/LogLWCController.getLogsLWC';
import {
    LOG_LWC_COLUMS,
    LOG_LWC_FIELDS,
    TOAST_EVENT_VARIANT_ERROR,
    showToast
} from 'c/constantLwcUtils';

export default class LogRecords extends LightningElement {
    @track logs;
    columns = LOG_LWC_COLUMS;
    fieldsString;

    connectedCallback() {
        this.getLogsLWC();
    }

    getLogsLWC() {
        this.fieldsString = LOG_LWC_FIELDS.join(", ");
        getLogsLWC({
            fields: this.fieldsString
        })
            .then(result => {
                this.logs = this.sortByRecords(result);
            })
            .catch(error => {
                showToast(TOAST_EVENT_VARIANT_ERROR, error.message.body, TOAST_EVENT_VARIANT_ERROR);
            });
    }

    sortByRecords(data) {
        let parseData = JSON.parse(JSON.stringify(data));
        return parseData.sort((a, b) => new Date(a.CreatedDate) > new Date(b.CreatedDate) ? -1 : 1);
    }
}