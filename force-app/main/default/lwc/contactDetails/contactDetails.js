import { LightningElement, wire, api } from 'lwc';
import { getRecord, getFieldValue } from "lightning/uiRecordApi";
import {CONTACT_FIRST_NAME_FIELD,
        CONTACT_LAST_NAME_FIELD,
        CONTACT_PHONE_FIELD,
        CONTACT_EMAIL_FIELD,
        CONTACT_TOTAL_PROPERTY_PRICE_FIELD,
        showToast,
        FAILED_GET_CONTACT
} from 'c/constantLwcUtils'

const fields = [
    CONTACT_FIRST_NAME_FIELD,
    CONTACT_LAST_NAME_FIELD,
    CONTACT_PHONE_FIELD,
    CONTACT_EMAIL_FIELD,
    CONTACT_TOTAL_PROPERTY_PRICE_FIELD 
];

export default class ContactDetails extends LightningElement {
    @api recordId;
    isSpinner
    First_Name__c;
    Last_Name__c;
    Phone;
    Email;
    Total_Property_Price__c

    @wire(getRecord, { recordId: '$recordId', fields })
    wiredRecord({ error, data }) {
        if (error) {
            this.isSpinner = false
            showToast(FAILED_GET_CONTACT, reduceErrors(error).join(', '));
        } else if (data) {
            fields.forEach(
                (item) => (this[item.fieldApiName] = getFieldValue(data, item))
            );
            this.isSpinner = false
        }
    }  
    connectedCallback(){
        this.isSpinner = true
    } 
}