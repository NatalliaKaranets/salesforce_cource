import { LightningElement, wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { reduceErrors } from 'c/ldsUtils';
import { subscribe, MessageContext } from 'lightning/messageService';
import {
	PROPERTY_NAME_FIELD,
	PROPERTY_PICTURE_URL_FIELD,
	PROPERTY_ADDRESS_FIELD,
	PROPERTY_RENT_COST_FIELD,
	PROPERTY_SELLING_PRICE_FIELD,
	PROPERTY_DESCRIPTION_FIELD,
	showToast,
	FAILED_GET_PROPERTY,
	TIMEOUT,
	STICKY,
	TOAST_TYPES
} from 'c/constantLwcUtils'
import PROPERTY_SELECTED_CHANNEL from '@salesforce/messageChannel/Property_Selected__c';

const fields = [
	PROPERTY_NAME_FIELD,
	PROPERTY_PICTURE_URL_FIELD,
	PROPERTY_ADDRESS_FIELD,
	PROPERTY_RENT_COST_FIELD,
	PROPERTY_SELLING_PRICE_FIELD,
	PROPERTY_DESCRIPTION_FIELD
];

export default class PropertyDetails extends LightningElement {
	isSpinner;
	timeout = TIMEOUT;
	sticky = STICKY;
	subscription = null;
	recordId;
	Name;
	Address__c;
	Picture_URL__c;
	Rent_Cost__c;
	Selling_price__c;
	Description__c;
	
	@wire(getRecord, { recordId: '$recordId', fields })
	wiredRecord({ error, data }) {
		if (error) {
			this.showToast(TOAST_TYPES.error, reduceErrors(error).join(', '));
		} else if (data) {
			fields.forEach(
				(item) => (this[item.fieldApiName] = getFieldValue(data, item))
			);
			this.isSpinner = false
		}
	}

	@wire(MessageContext)
	messageContext;

	subscribeToMessageChannel() {
		this.subscription = subscribe(
			this.messageContext,
			PROPERTY_SELECTED_CHANNEL,
			(message) => this.handleMessage(message)
		);
	}

	handleMessage(message) {
		this.isSpinner = true
		this.recordId = message.recordId;
	}

	connectedCallback() {
		this.subscribeToMessageChannel();
	}
	showToast(type, message) {
		this.template.querySelector("c-error-handler")
			.showToast(type, message)
	}

}
