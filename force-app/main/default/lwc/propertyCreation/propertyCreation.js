import { LightningElement, api, wire, track } from 'lwc'
import propertyCreationTemplate from './propertyCreation.html'
import propertyCreationFormTemplate from './propertyCreationForm.html'
import createLogLWC from '@salesforce/apex/LogLWCController.createLogLWC'
import { getObjectInfo } from 'lightning/uiObjectInfoApi'
import PROPERTY_OBJECT from '@salesforce/schema/Property__c'
import {
	TOAST_EVENT_VARIANT_SUCCESS,
	TOAST_EVENT_VARIANT_ERROR,
	LOGLWC_ACTION_TYPE_INSERT,
	TOAST_EVENT_PROPERTY_CREATED,
	RECORD_TYPE_INDUSTRIAL,
	PROPERTY_CREATE_FAILED,
	showToast
} from 'c/constantLwcUtils'

export default class PropertyCreation extends LightningElement {
	@api recordId
	@wire(getObjectInfo, { objectApiName: PROPERTY_OBJECT })
	objectInfo
	@track propertyes = []
	@track forms = [1]
	isPropertyRecordType = true
	currentRecordTypeId
	disableDelete = true
	disableAdd = false
	lastNumber = 1
	recordTypeId
	propertyOwner = window.location.pathname.split('/')[4]

	get isNextDisable() {
		return !this.currentRecordTypeId
	}

	get propertyApiName() {
		return PROPERTY_OBJECT.objectApiName
	}
	
	connectedCallback() {
		setTimeout(() => {
			const propertyes = this.objectInfo.data.recordTypeInfos
			Object.keys(propertyes).forEach((element, index ) => {
				if(!propertyes[element].master) {
					this.propertyes.push({
						value: propertyes[element].name,
						recordTypeId: propertyes[element].recordTypeId,
						checked: propertyes[element].name === RECORD_TYPE_INDUSTRIAL
					})
					this.currentRecordTypeId = this.propertyes[0].recordTypeId
				}
			})
		}, 1000)
	}

	render() {
		return this.isPropertyRecordType
		  ? propertyCreationTemplate
		  : propertyCreationFormTemplate
	}
	

	handleChangeRecordType(event){
		const valueRecordType = event.target.name
		if (valueRecordType != this.currentRecordTypeId) {
			this.currentRecordTypeId = valueRecordType
			this.propertyes.forEach(property => {
				property.recordTypeId === valueRecordType ? property.checked = 1 : property.checked = 0
			})
		} else { 
			this.currentRecordTypeId = null
		}
	}

	onChangeTemplate () {
		this.isPropertyRecordType = this.isPropertyRecordType ? false : true
	}

	handleSubmit(){
		try {
			this.template.querySelectorAll('lightning-record-edit-form').forEach(form => form.submit())
			this.onChangeTemplate()
			showToast(TOAST_EVENT_VARIANT_SUCCESS, TOAST_EVENT_PROPERTY_CREATED, TOAST_EVENT_VARIANT_SUCCESS)
			this.createLog(LOGLWC_ACTION_TYPE_INSERT)
		} catch (error){
			showToast(TOAST_EVENT_VARIANT_ERROR, error, TOAST_EVENT_VARIANT_ERROR)
			this.createLog(LOGLWC_ACTION_TYPE_INSERT, error.body.message)
		}
	}
		
	handleAdd() {
		++this.lastNumber
		if (this.forms.length === 1){
			this.forms.push(this.lastNumber)
			this.disableDelete = false
		} else {
			this.forms.push(this.lastNumber)
			this.disableAdd = true
		}
	}
		
	handleDelete(event) {
		if (this.forms.length > 1){
			this.disableDelete = this.forms.length === 2
			this.disableAdd = this.forms.length === 3
			this.forms = this.forms.filter(item => {
				return item != parseInt(event.target.accessKey)
			})
		}
	}

	createLog(type, error) {
		let currentProperty = this.propertyes.find(item => item.recordTypeId === this.currentRecordTypeId)
		createLogLWC({
			objectType: PROPERTY_OBJECT.objectApiName,
			actionType: type,
			description: error
				? PROPERTY_CREATE_FAILED
				: this.forms.length + ' record(s) created with ' + currentProperty.value + ' recordType',
			logErrorMessage: error ? error : ''
		})	
	}
}