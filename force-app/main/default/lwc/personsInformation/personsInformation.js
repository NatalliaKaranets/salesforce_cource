import { LightningElement } from 'lwc'

import {
    persons,
    personInformationColumns,
} from "c/constantLwcUtils";

export default class PersonsInformation extends LightningElement {
	data = persons
	columns = personInformationColumns
	value = 'sortBy'
	currentGender = null
	currentEmail = null
	optionsGender = [
		{ label: 'Male', value: 'MALE', checked: null },
		{ label: 'Female', value: 'FEMALE', checked: null },
	]

	get options() {
		return [
			{ label: 'First Name', value: 'firstName' },
			{ label: 'Last Name', value: 'lastName' },
			{ label: 'Email', value: 'email' },
		]
	}

	handleChangeSort(event) {
		this.value = event.detail.value
		const result = JSON.parse(JSON.stringify(this.data)).sort((a, b) => a[event.detail.value].localeCompare(b[event.detail.value]))
		this.data = result
	}

	handleChangeFrom(event){
		if (event.detail.value) {
			const choosedDate = new Date(event.detail.value)
			const result = this.data.filter(item => {
				return new Date(item.birthday) >= choosedDate
			})
			this.data = result
		}else {
			this.data = persons
			this.filterByGender()
			this.filterByEmail()
		}
	}

	handleChangeTo(event){
		if (event.detail.value) {
		const choosedDate = new Date(event.detail.value)
		const result = this.data.filter(item => {
			return new Date(item.birthday) <= choosedDate
		})
		this.data = result
	} else {
		this.data = persons
		this.filterByGender()
		this.filterByEmail()
	}
}

	handleChangeGender(event){
		const valueGender = event.target.name
		if (valueGender != this.currentGender) {
			this.currentGender = valueGender
			this.optionsGender.forEach(gender => {
				gender.value === valueGender ? gender.checked = 1 : gender.checked = 0
			})
			this.filterByGender()
		} else { 
			this.currentGender = null
			this.data = persons 
		}
	}

	filterByGender() {
		if (this.currentGender) {
			const result = persons.filter(item => {
				return item.gender === this.currentGender
			})
			this.data = result
		}else {
			this.data = persons
		}
	}

	handleChangeEmail(event){
		this.filterByGender()
		this.currentEmail = event.target.value
		this.filterByEmail () 
	}

	filterByEmail(){
		if (this.currentEmail){
			const result = this.data.filter(item => {
				return item.email.toLowerCase().includes(this.currentEmail.toLowerCase())
			})
			this.data = result
		} 
	}

	handleReset(){
		this.value = ''
		this.template.querySelectorAll('lightning-input').forEach(element => {
			if (element.type === 'checkbox'){
				element.checked = false
			} else {
				element.value = null
			}
			this.currentGender = null
			this.currentEmail = null
			this.data = persons
		})
	}
}