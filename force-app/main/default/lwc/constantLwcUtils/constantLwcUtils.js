import { ShowToastEvent } from "lightning/platformShowToastEvent";
import LOG_OBJECT_TYPE_FIELD from '@salesforce/schema/LogLWC__c.ObjectType__c';
import LOG_ACTION_TYPE_FIELD from '@salesforce/schema/LogLWC__c.ActionType__c';
import LOG_DESCRIPTION_FIELD from '@salesforce/schema/LogLWC__c.CreatedDate';
import LOG_CREATE_DATE_FIELD from '@salesforce/schema/LogLWC__c.Description__c';
import LOG_IS_SECCESSFUL_FIELD from '@salesforce/schema/LogLWC__c.IsSuccessful__c';
import LOG_ERROR_MESSAGE_FIELD from '@salesforce/schema/LogLWC__c.ErrorMessage__c';
import PROPERTY_ID from '@salesforce/schema/Property__c.Id';
import PROPERTY_NAME_FIELD from '@salesforce/schema/Property__c.Name';
import PROPERTY_ADDRESS_FIELD from '@salesforce/schema/Property__c.Address__c';
import PROPERTY_PICTURE_URL_FIELD from '@salesforce/schema/Property__c.Picture_URL__c';
import PROPERTY_RENT_COST_FIELD from '@salesforce/schema/Property__c.Rent_Cost__c';
import PROPERTY_SELLING_PRICE_FIELD from '@salesforce/schema/Property__c.Selling_price__c';
import PROPERTY_DESCRIPTION_FIELD from '@salesforce/schema/Property__c.Description__c';
import FAILD_GET_PROPERTY from  '@salesforce/label/c.Faild_get_property';
import EMPTY_PROPERTY_LIST from  '@salesforce/label/c.Empty_property_list';
import PROPERTY_OWNER_FIELD from '@salesforce/schema/Property__c.Property_Owner__c';
import CONTACT_FIRST_NAME_FIELD from '@salesforce/schema/Contact.First_Name__c';
import CONTACT_LAST_NAME_FIELD from '@salesforce/schema/Contact.Last_Name__c';
import CONTACT_PHONE_FIELD from '@salesforce/schema/Contact.Phone';
import CONTACT_EMAIL_FIELD from '@salesforce/schema/Contact.Email';
import CONTACT_TOTAL_PROPERTY_PRICE_FIELD from '@salesforce/schema/Contact.Total_Property_Price__c';

const LOG_LWC_COLUMS = [
	{ label: 'ObjectType', fieldName: 'ObjectType__c' },
	{ label: 'ActionType', fieldName: 'ActionType__c' },
	{ label: 'Description', fieldName: 'Description__c' },
	{ label: 'IsSuccessful', fieldName: 'IsSuccessful__c' },
	{ label: 'ErrorMessage', fieldName: 'ErrorMessage__c' },
	{ label: 'CreatedDate', fieldName: 'CreatedDate' },
];

const LOG_LWC_FIELDS = [
    LOG_OBJECT_TYPE_FIELD.fieldApiName,
    LOG_ACTION_TYPE_FIELD.fieldApiName,
    LOG_DESCRIPTION_FIELD.fieldApiName,
    LOG_CREATE_DATE_FIELD.fieldApiName,
    LOG_IS_SECCESSFUL_FIELD.fieldApiName,
    LOG_ERROR_MESSAGE_FIELD.fieldApiName, 
];

const PROPERTY_FIELDS = [
    PROPERTY_ID.fieldApiName,
    PROPERTY_NAME_FIELD.fieldApiName,
    PROPERTY_PICTURE_URL_FIELD.fieldApiName,
    PROPERTY_RENT_COST_FIELD.fieldApiName,
    PROPERTY_SELLING_PRICE_FIELD.fieldApiName,
    PROPERTY_ADDRESS_FIELD.fieldApiName,
    PROPERTY_DESCRIPTION_FIELD.fieldApiName
];

const personInformationColumns = [
    { label: 'Last Name', fieldName: 'lastName', type: 'text' },
    { label: 'First Name', fieldName: 'firstName', type: 'text'},
    { label: 'Gender', fieldName: 'gender' , type: 'text'},
    { label: 'Birthday', fieldName: 'birthday', type: 'date' },
    { label: 'Email', fieldName: 'email', type: 'email' },
];
    
const TOAST_TYPES = {
    error: 'error'
};

const MESSAGE_TYPES = {
    faildGetProperty: FAILD_GET_PROPERTY,
    emptyPropertyList: EMPTY_PROPERTY_LIST
};

const STICKY = false;
const TIMEOUT = 3000;

const persons = [
	{Id:1, lastName: 'Smith', firstName: 'Jon', gender: 'MALE', birthday: '01.01.2000', email: 'Smith@com'},
	{Id:2, lastName:'Johnson', firstName: 'Chack', gender: 'MALE', birthday: '01.02.1996', email: 'Johnson@com'},
	{Id:3, lastName: 'Williams', firstName: 'Sam', gender: 'MALE', birthday: '11.06.1988', email: 'Williams@com'},
	{Id:4, lastName: 'Brown', firstName: 'Sara', gender: 'FEMALE', birthday: '05.03.1995', email: 'Brown@com'},
	{Id:5, lastName: 'Jones', firstName: 'Eva', gender: 'FEMALE', birthday: '04.04.1994', email: 'Jones@com'},
	{Id:6, lastName: 'Garcia', firstName: 'Liam', gender: 'MALE', birthday: '06.07.1992', email: 'Garcia@com'},
	{Id:7, lastName: 'Miller', firstName: 'Oliver', gender: 'MALE', birthday: '11.08.1975', email: 'Miller@com'},
	{Id:8, lastName: 'Davis', firstName: 'James', gender: 'MALE', birthday: '12.05.1976', email: 'Davis@com'},
	{Id:9, lastName: 'Rodriguez', firstName: 'William', gender: 'MALE', birthday: '12.02.1982', email: 'Rodriguez@com'},
	{Id:10, lastName: 'Wilson', firstName: 'Emma', gender: 'FEMALE', birthday: '08.07.1993', email: 'Wilson@com'},
	{Id:11, lastName: 'Anderson', firstName: 'Mila', gender: 'FEMALE', birthday: '12.09.1979', email: 'Anderson@com'},
	{Id:12, lastName: 'Taylor', firstName: 'Charlotte', gender: 'FEMALE', birthday: '01.06.1991', email: 'Taylor@com'},
];

const showToast = (title, msg, variant) => {
	const evt = new ShowToastEvent({
		title: title,
		message: msg,
		variant: variant || 'error'
	});
	dispatchEvent(evt);
};

const TOAST_EVENT_VARIANT_SUCCESS = 'success';
const TOAST_EVENT_VARIANT_ERROR = 'error';
const LOGLWC_ACTION_TYPE_INSERT = 'Insert';
const TOAST_EVENT_PROPERTY_CREATED = 'Property created';
const RECORD_TYPE_INDUSTRIAL = 'Industrial';
const PROPERTY_CREATE_FAILED = 'Failed to create property entry';
const FAILED_GET_PROPERTY = 'Failed to get property';
const FAILED_GET_CONTACT = 'Failed to get contact';


export  {
	persons,
	personInformationColumns,
	showToast,
	LOG_LWC_COLUMS,
	LOG_LWC_FIELDS,
	LOG_OBJECT_TYPE_FIELD,
	LOG_ACTION_TYPE_FIELD,
	LOG_DESCRIPTION_FIELD,
	LOG_CREATE_DATE_FIELD,
	LOG_IS_SECCESSFUL_FIELD,
	LOG_ERROR_MESSAGE_FIELD, 
	TOAST_EVENT_VARIANT_SUCCESS,
	TOAST_EVENT_VARIANT_ERROR,
	LOGLWC_ACTION_TYPE_INSERT,
	TOAST_EVENT_PROPERTY_CREATED,
	RECORD_TYPE_INDUSTRIAL,
	PROPERTY_CREATE_FAILED,
    TOAST_TYPES,
    MESSAGE_TYPES,
    STICKY,
    PROPERTY_FIELDS,
    TIMEOUT,
	FAILED_GET_PROPERTY,
	PROPERTY_NAME_FIELD,
	PROPERTY_PICTURE_URL_FIELD,
	PROPERTY_ADDRESS_FIELD,
	PROPERTY_RENT_COST_FIELD,
	PROPERTY_SELLING_PRICE_FIELD,
	PROPERTY_DESCRIPTION_FIELD,
	PROPERTY_OWNER_FIELD,
	CONTACT_FIRST_NAME_FIELD,
	CONTACT_LAST_NAME_FIELD,
	CONTACT_PHONE_FIELD,
	CONTACT_EMAIL_FIELD,
	CONTACT_TOTAL_PROPERTY_PRICE_FIELD,
	FAILED_GET_CONTACT
};
