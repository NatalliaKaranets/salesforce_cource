import {LightningElement, wire} from 'lwc';
import { reduceErrors } from 'c/ldsUtils';
import getAllProperties from '@salesforce/apex/PropertyManager.getAllProperties';
import { PROPERTY_FIELDS, STICKY, TIMEOUT, TOAST_TYPES, MESSAGE_TYPES } from 'c/constantLwcUtils';
import { publish, MessageContext } from 'lightning/messageService';
import PROPERTY_SELECTED_CHANNEL from '@salesforce/messageChannel/Property_Selected__c';



export default class PropertyList extends LightningElement {
	totalProperties
	visibleProperties
	isSpinner
	timeout = TIMEOUT
	sticky = STICKY

	@wire(getAllProperties, { 
		fields: PROPERTY_FIELDS.join(", ")
	})

	wiredProperties({ error, data }) {
		this.isSpinner = true
		if (data) {
			this.totalProperties = []
			Object.keys(data).forEach(element => {
				this.totalProperties.push({ 
					id: data[element].Id,
					name: data[element].Name,
					image: data[element].Picture_URL__c,
					price: data[element].Selling_price__c,
					rent: data[element].Rent_Cost__c,
					address: data[element].Address__c,

				})
			})
			if (this.totalProperties.length < 1) this.showToast(TOAST_TYPES.error, MESSAGE_TYPES.emptyPropertyList)
            this.isSpinner = false
		}
		else if (error) {
			this.showToast(TOAST_TYPES.error, reduceErrors(error).join(', '))
			this.totalProperties = []
		}
	}    

	@wire(MessageContext)
    messageContext;

    handlePropertySelect(event) {
        const payload = { recordId: event.target.property.id };
        publish(this.messageContext, PROPERTY_SELECTED_CHANNEL, payload);
    }

	updatePropertiesHandler(event){
		this.visibleProperties=[...event.detail.records]
	}

	updateSpinnerHandler(event){
		this.isSpinner=event.detail.records
	}

	showToast(type, message) {
		this.template.querySelector("c-error-handler")
			.showToast(type, message)
	}

}