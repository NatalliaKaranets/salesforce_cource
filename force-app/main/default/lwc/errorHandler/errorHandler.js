import { LightningElement, api, track } from 'lwc'

export default class ErrorHandler extends LightningElement {
	@track toastList = []
	@track toastId = 0
	@api timeout
	@api sticky
	@api
	showToast(type, message) {
	  let toast = {
		type: type,
		headerMessage: type,
		message: message,
		id: this.toastId,
		iconName: "utility:" + type,
		headerClass: "slds-notify slds-notify_toast slds-theme_" + type
	  }
	  this.toastId = this.toastId + 1
	  this.toastList.push(toast)
  
	  if (this.sticky === false) {
		setTimeout(() => {
		  this.closeToast()
		}, this.timeout)
	  }
	}
	closeToast() {
	  let index = this.toastId - 1
	  if (index != -1) {
		this.filerToastList(index)
	  }
	}
	handleClose(event) {
	  let index = event.target.dataset.index
	  if (index != -1) {
		this.filerToastList(index)
	  }
	}
	filerToastList(index) {
		this.toastList.splice(index, 1)
		this.toastId = this.toastId - 1
	}
}